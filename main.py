#!/usr/bin/env python
import os
import jinja2
import webapp2

template_dir = os.path.join(os.path.dirname(__file__), "templates")
jinja_env = jinja2.Environment(loader=jinja2.FileSystemLoader(template_dir), autoescape=False)


class BaseHandler(webapp2.RequestHandler):

    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

    def render_str(self, template, **params):
        t = jinja_env.get_template(template)
        return t.render(params)

    def render(self, template, **kw):
        self.write(self.render_str(template, **kw))

    def render_template(self, view_filename, params=None):
        if not params:
            params = {}
        template = jinja_env.get_template(view_filename)
        self.response.out.write(template.render(params))


characteristics = {

    'hair color': {
        'black': 'CCAGCAATCGC',
        'brown': 'GCCAGTGCCG',
        'carrot': 'TTAGCTATCGC'
    },

    'face shape': {
        'square': 'GCCACGG',
        'round': 'ACCACAA',
        'oval': 'AGGCCTCA'
    },

    'eye color': {
        'blue': 'TTGTGTGGC',
        'brown': 'AAGTAGTGAC',
        'green': 'GGGAGGTGGC'
    },

    'gender': {
        'male': 'TGCAGGAACTTC',
        'female': 'TGAAGGACCTTC'
    },

    'race': {
        'white': 'AAAACCTCA',
        'black': 'CGACTACAG',
        'asian': 'CGCGGGCCG'
    }
}


class MainHandler(BaseHandler):
    def get(self):
        return self.render_template("hello.html")

    def post(self):
        dna = self.request.get("enter_dna").upper()
        result = {}

        for characteristic, values in characteristics.items():
            for value, sequence in values.items():
                if dna.find(sequence) != -1:
                    result[characteristic.capitalize()] = value.capitalize()
            if characteristic.capitalize() not in result:
                result[characteristic.capitalize()] = 'Couldn\'t find a gene for {}.'.format(characteristic)

        params = {
            'result': result
        }

        return self.render_template("result.html", params=params)


app = webapp2.WSGIApplication([
    webapp2.Route('/', MainHandler),
    webapp2.Route('/result', MainHandler)
], debug=True)
